Namespace("Layouts",  {
    defaultMain: Meteor.settings.public.layouts.masterLayout,
    defaultBlank: Meteor.settings.public.layouts.defaultBlank
});

Namespace("Routes", {
    exposed: FlowRouter.group({}),
    secured: FlowRouter.group({
        triggersEnter: [
            function () {
                var route;
                if (!(Meteor.loggingIn() || Meteor.userId())) {
                    route = FlowRouter.current();

                    if (route.route.name !== 'login' && route.route.name !== 'register') {
                        Session.set('redirectAfterLogin', route.path);
                        Session.set('loggedIn', false);
                        FlowRouter.go('login');
                    }

                    return true;
                }
            }
        ]
    }),
});

Namespace("Routes", {
    admin: App.Routes.secured.group({
        prefix: "/admin",
        triggersEnter: [
            function () {
                if (!Roles.userIsInRole(Meteor.user(), ['admin'])) {
                    return FlowRouter.go(FlowRouter.path('dashboard'));
                }
            }
        ]
    })
})

// Admin Routes
App.Routes.admin.route('/users', {
    name: "user-list",
    action: function () {
        BlazeLayout.render(App.Layouts.defaultMain, {content: 'UserList'});
    }
});

// Secured Routes
App.Routes.secured.route('/dashboard', {
    name: "dashboard",
    action: function () {
        BlazeLayout.render(App.Layouts.defaultMain, {content: 'Dashboard'});
    }
});

App.Routes.secured.route('/edit-profile', {
    name: "edit-profile",
    action: function () {
        BlazeLayout.render(App.Layouts.defaultMain, {content: 'editUser'});
    }
});

// Exposed Routes
App.Routes.exposed.route('/login', {
    name: "login",
    action: function () {
        BlazeLayout.render(App.Layouts.defaultBlank, {content: 'Login'});
    }
});

App.Routes.exposed.route('/register', {
    name: "register",
    action: function () {
        BlazeLayout.render(App.Layouts.defaultBlank, {content: 'Register'});
    }
});

App.Routes.secured.route('/', {
    name: "default",
    action: function () {
        BlazeLayout.render(App.Layouts.defaultMain, {content: 'default'});
    }
});

// Not Found
FlowRouter.notFound = {
    action: function () {
        BlazeLayout.render(App.Layouts.defaultBlank, {content: 'notFound'});
    }
};

FlowRouter.triggers.enter(function(){
    Session.set("errors", []);
});

