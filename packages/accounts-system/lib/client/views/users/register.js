Template.Register.events({
    'submit #register-form': function (event, template) {
        event.preventDefault();
        Session.set("errors", []);

        // 1. Collect the username and password from the form
        var first_name = template.find('#first_name').value,
            last_name = template.find('#last_name').value,
            email = template.find('#email').value,
            password = template.find('#password').value,
            password_confirmation = template.find('#password_confirmation').value;

        var default_plan = App.Model.Accounts.Plans.collection.find({plan_name: "Free"}).fetch();

        var user = new App.Model.Accounts.Person.entity({
            emailAddress: email,
            password: password,
            password_confirmation: password_confirmation,
            profile: {
                first_name: first_name,
                last_name: last_name,
                display_name: first_name + " " + last_name
            },
            plan: default_plan[0].plan_id
        });

        Meteor.call("/users/validate", user, function(error, result) {
            if(error) {
                console.log(error);
                var message = new App.Model.Core.Message.entity(error);
                var errors = [message];
                Session.set("errors", errors);
            }
            else if(result.errors.length) {
                Session.set("errors", result.errors);

                for(field in result.error_fields) {
                    var form_group = template.find('#' + result.error_fields[field]).closest('.form-group');
                    $(form_group).addClass('has-error');
                }
            }
        });

        if(!Session.get("errors").length) {
            var card = {}, card_token, customer;

            Meteor.call("stripeCreateCardToken", card, function(error, result) {
                user.token = result.id;

                Meteor.call("createTrialCustomer", user, function(error, newCustomer){
                    if(error) {
                        throw new Meteor.Error(error)
                    }
                    else {

                        Meteor.loginWithPassword(user.emailAddress, user.password, function(error){
                            if(error) {
                                var message = new App.Model.Core.Message.entity(error);
                                var errors = [message];
                                Session.set("errors", errors);
                            }
                            else {
                                Session.set('loggedIn', true);
                                Session.set("errors", []);
                                FlowRouter.go('/dashboard');
                            }
                        });
                    }
                });
            })
        }

        return false;
    }
});
