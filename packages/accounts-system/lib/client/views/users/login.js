Accounts.onLogin(function() {
    var redirect;
    redirect = Session.get('redirectAfterLogin');
    if (redirect != null) {
        if (redirect !== '/login') {
            return FlowRouter.go(redirect);
        }
    }
});

Tracker.autorun(function() {
    if (!Meteor.userId()) {
        if (Session.get('loggedIn')) {
            var route = FlowRouter.current();
            Session.set('redirectAfterLogin', route.path);
            FlowRouter.go(FlowRouter.path('login'));
        }
    }
});

Template.Login.events({
    'submit #login-form': function (event, template) {
        event.preventDefault();
        Session.set("errors", []);
        Session.set('loggedIn', false);

        // 1. Collect the username and password from the form
        var username = template.find('#login-username').value,
            password = template.find('#login-password').value;

        Meteor.loginWithPassword(username, password, function(error){
            if(error) {
                var message = new App.Model.Core.Message.entity(error);
                var errors = [message];
                Session.set("errors", errors);
            }
            else {
                Session.set('loggedIn', true);
                Session.set("errors", []);
                FlowRouter.go('/dashboard');
            }
        });

        return false;
    }
});
