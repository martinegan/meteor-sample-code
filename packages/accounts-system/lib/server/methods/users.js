Meteor.methods({
	'/users/validate': function(user) {
		var errors = [], error, error_fields = [];

		if (!user.profile.first_name) {
			error_fields.push("first_name");
			error = new App.Model.Core.Message.entity({
				reason: 'Please include a first name.'
			});
			errors.push(error);
		}

		if (!user.profile.last_name) {
			error_fields.push("last_name");
			error = new App.Model.Core.Message.entity({
				reason: 'Please include a last name.'
			});
			errors.push(error);
		}


		if (!user.emailAddress) {
			error_fields.push("email");
			error = new App.Model.Core.Message.entity({
				reason: 'Please include an email'
			});
			errors.push(error);
		}


		if (!user.password) {
			error_fields.push("password");
			error_fields.push("password_confirmation");
			error = new App.Model.Core.Message.entity({
				reason: 'Please provide a password.'
			});
			errors.push(error);
		}


		if(user.password !== user.password_confirmation) {
			error_fields.push("password");
			error_fields.push("password_confirmation");
			error = new App.Model.Core.Message.entity({
				reason: 'Passwords must match.'
			});
			errors.push(error);
		}

		return {errors: errors, error_fields: error_fields};
	}
})
