Accounts.onCreateUser(function(options, user) {
	// Use provided profile in options, or create an empty object
	if (options.profile) {
		user.profile = options.profile;
	}

	// Returns the user object
	return user;
});