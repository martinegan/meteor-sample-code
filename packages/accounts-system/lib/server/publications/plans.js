Meteor.publish('plans', function() {
	return App.Model.Accounts.Plans.collection.find({active: true});
});