Namespace("Model.Accounts.Plans", {
	defaults: {

	}
});

class Plan extends App.Model.Core.Base.entity {
	constructor(plan) {
		super();
		_.extend(this, App.Model.Accounts.Plans.defaults, plan);
	}
}

Namespace("Model.Accounts.Plans", {"entity": Plan, "collection": new Meteor.Collection("Plans")});

if(Meteor.isServer) {
	if(App.Model.Accounts.Plans.collection.find().fetch().length < 1) {
		var plans = [
			{plan_id: "free_plan", plan_name: "Free", description: "Our Free Plan", cost: "0.00", currency: "GBP"},
			{plan_id: "paid_plan", plan_name: "Paid", description: "Our Paid Plan", cost: "10.00", currency: "GBP", frequency: "Monthly"},
		];

		for(plan in plans) {
			App.Model.Accounts.Plans.collection.insert(new App.Model.Accounts.Plans.entity(plans[plan]));
		}
	}
}
