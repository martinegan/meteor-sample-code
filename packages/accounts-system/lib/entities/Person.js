Namespace("Model.Accounts.Person", {
	defaults: {

	}
});

class Person extends App.Model.Core.Base.entity {
	constructor(person) {
		super();
		_.extend(this, App.Model.Accounts.Person.defaults, person);
	}
}

Namespace("Model.Accounts.Person", {
	"entity": Person,
	"collection": Meteor.users,
	"helpers": {
		plan: function(user) {
			if(user.profile.planId) {
				return App.Model.Accounts.Plans.collection.findOne(user.profile.planId);
			}
			else {
				return [];
			}
		}

	}
});
