Package.describe({
  name: 'meteordemo:accounts-system',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.use('meteordemo:core');
  api.imply('meteordemo:core');

  var client_config = [
    'lib/client/config/stripe.config.js'
  ];

  var views = [
    'lib/client/views/users/login.html',
    'lib/client/views/users/login.js',
    'lib/client/views/users/register.html',
    'lib/client/views/users/register.js',
    'lib/client/views/users/editUser.html',
    'lib/client/views/users/editUser.js'
  ];

  var entities = [
    'lib/entities/Plan.js',
    'lib/entities/Person.js'
  ];

  var server = [
    'lib/server/accounts.js',
    'lib/server/methods/stripe.js',
    'lib/server/methods/users.js',
    'lib/server/methods/signup.js',
    'lib/server/publications/plans.js',
    'lib/server/publications/users.js'
  ];

  // Templates
  api.addFiles(client_config, ['client']);
  api.addFiles(views, ['client']);
  api.addFiles(entities, ['client', 'server']);
  api.addFiles(server, ['server']);
});
