Package.describe({
  name: 'meteordemo:lib',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: null
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');

  var packages = [
    'blaze-html-templates',
    'meteor-platform',
    'ecmascript',
    'es5-shim',
    'less',
    'jquery',
    'meteor-base',
    'mobile-experience',
    'mongo',
    'session',
    'standard-minifiers',
    'tracker',
    'twbs:bootstrap',
    'fortawesome:fontawesome',
    'sacha:spin',
    'tmeasday:publish-counts',
    'percolate:find-from-publication',
    'aldeed:collection2',
    'zephraph:namespace',
    'alanning:roles',
    'check',
    'http',
    'spiderable',
    'service-configuration',
    'email',
    'aldeed:template-extension',
    'reactive-var',
    'underscore',
    'markdown',
    'kadira:flow-router',
    'arillo:flow-router-helpers',
    'rochal:slimscroll',
    'kadira:blaze-layout',
    'ongoworks:bunyan-logger',
    'ongoworks:security',
    'aldeed:simple-schema',
    'blaze-html-templates',
    'meteordemo:namespacer',
    'accounts-password',
    'zimme:active-route',
    'fastclick',
    'mizzao:jquery-ui',
    'mrgalaxy:stripe',
    'momentjs:moment'
  ];

  api.use(packages);
  api.imply(packages);
});
