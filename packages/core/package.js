Package.describe({
  name: 'meteordemo:core',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');

  var packages = [
    'meteordemo:lib@0.0.1',
  ];

  api.use(packages);
  api.imply(packages);

  var entities = [
    'lib/entities/Base.js',
    'lib/entities/Message.js'
  ];

  var views = [
    'lib/client/views/messages/alert.html',
    'lib/client/views/messages/alert.js'
  ];

  api.addFiles(entities, ['client', 'server']);
  api.addFiles(views, ['client']);

});
