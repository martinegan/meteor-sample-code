Meteor.startup(function() {
	Session.setDefault("errors", null);
});

Template.alert.helpers({
	alertMessages: function() {
		return Session.get("errors");
	},
	hasAlerts: function() {
		return Array.isArray(Session.get("errors")) && Session.get("errors").length;
	}
})