Namespace("Model.Core.Base", {
    defaults: {
        active: true,
        deleted: false,
        createdAt: new Date(),
        modifiedAt: new Date(),
        createdBy: 0,
        modifiedBy: 0,
        displayOrder: 0
    }
})

class Base {
    constructor() {
        _.extend(this, App.Model.Core.Base.defaults);
    }

    get(key) {
        return this[key];
    }
}

Namespace("Model.Core.Base", {"entity": Base});
