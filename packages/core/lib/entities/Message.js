Namespace("Model.Core.S", {
	defaults: {
		group: "Global",
		message: "",
		action_link: "",
		type: "Info"
	}
})

class Message {
	constructor(message) {
		_.extend(this, App.Model.Core.Message.defaults);
		_.extend(this, message);
	}
}

Namespace("Model.Core.Message", {"entity": Message});
