Package.describe({
    name: 'cornerstonedigital:smart-admin-theme',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: '',
    // URL to the Git repository containing the source code for this package.
    git: '',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

function getFilesFromFolder(packageName, folder) {
    // local imports
    var _ = Npm.require("underscore");
    var fs = Npm.require("fs");
    var path = Npm.require("path");
    // helper function, walks recursively inside nested folders and return absolute filenames
    function walk(folder) {
        var filenames = [];
        // get relative filenames from folder
        var folderContent = fs.readdirSync(folder);
        // iterate over the folder content to handle nested folders
        _.each(folderContent, function (filename) {
            // build absolute filename
            var absoluteFilename = folder + path.sep + filename;
            // get file stats
            var stat = fs.statSync(absoluteFilename);
            if (stat.isDirectory()) {
                // directory case => add filenames fetched from recursive call
                filenames = filenames.concat(walk(absoluteFilename));
            }
            else {
                // file case => simply add it
                filenames.push(absoluteFilename);
            }
        });
        return filenames;
    }

    // save current working directory (something like "/home/user/projects/my-project")
    var cwd = process.cwd();
    // chdir to our package directory
    //console.log("../../packages" + path.sep + packageName);
    process.chdir("packages" + path.sep + packageName);
    // launch initial walk
    var result = walk(folder);
    // restore previous cwd
    process.chdir(cwd);
    return result;
};

Package.onUse(function (api) {
    api.versionsFrom('1.2.0.4-logging.0');
    var packages = [
        'meteordemo:lib@0.0.1',
    ];

    api.use(packages);
    api.imply(packages);

    var images = getFilesFromFolder("smart-admin-theme", "lib/public/assets/img");
    api.addAssets(images, ['client']);

    api.addFiles([
        'lib/config/SmartAdmin.utils.js',
        'lib/config/SmartAdmin.config.js',
        'lib/config/SmartAdmin.core.js',
        'lib/config/stripe.config.js',
        'smart-admin-theme.js'
    ], ['client']);

    var javascript = [
        'lib/public/assets/js/smartwidgets/jarvis.widget.js',
        'lib/public/assets/js/notification/SmartNotification.js',
        //'lib/public/assets/js/smart-chat-ui/smart.chat.manager.js',
        //'lib/public/assets/js/smart-chat-ui/smart.chat.ui.js',
        'lib/public/assets/js/speech/voicecommand.js'
    ];

    api.addFiles(javascript, ['client']);

    var sounds = getFilesFromFolder("smart-admin-theme", "lib/public/assets/sound");
    api.addAssets(sounds, ['client']);

    // Styles
    api.addFiles([
        'lib/public/assets/css/smart-admin.css',
        'lib/public/assets/css/smart-admin-plugins.css'
    ], ['client']);

    // Admin Files
    api.addFiles([
        'lib/views/admin/common/SmartAdminHeader.html',
        'lib/views/admin/common/SmartAdminHeader.js',
        'lib/views/admin/common/SmartAdminNavigation.html',
        'lib/views/admin/common/SmartAdminNavigation.js',
        'lib/views/admin/common/SmartAdminFooter.html',
        'lib/views/admin/common/SmartAdminFooter.js',
        'lib/views/admin/common/SmartAdminShortcut.html',
        'lib/views/admin/common/SmartAdminShortcut.js',
        'lib/views/admin/layouts/SmartAdminLayout.html',
        'lib/views/admin/layouts/SmartAdminLayout.js'
    ], ['client']);

    // FrontEnd Files
    api.addFiles([
        'lib/views/frontend/common/SmartAdminMainHeader.html',
        'lib/views/frontend/layouts/SmartAdminMainLayout.html',
        'lib/views/frontend/layouts/SmartAdminMainLayout.js'
    ], ['client']);
});
