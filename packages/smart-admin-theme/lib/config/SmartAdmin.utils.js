Namespace("SmartAdmin.Utils", {
    calc_navbar_height: function () {
        var height = null;

        if ($('#header').length)
            height = $('#header').height();

        if (height === null)
            height = $('<div id="header"></div>').height();

        if (height === null)
            return 49;
        // default
        return height;
    },
    drawBreadCrumb: function (bread_crumb, opt_breadCrumbs) {
        if (opt_breadCrumbs != undefined) {
            $.each(opt_breadCrumbs, function (index, value) {
                $(bread_crumb).append($("<li></li>").html(value));
                document.title = $(bread_crumb).find("li:last-child").text();
            });
        }
    },
    loadURL: function (url, container) {

        // debugState
        if (App.SmartAdmin.Config.Global.debugState) {
            root.root.console.log("Loading URL: %c" + url, debugStyle);
        }

        $.ajax({
            type: "GET",
            url: url,
            dataType: 'html',
            cache: true, // (warning: setting it to false will cause a timestamp and will call the request twice)
            beforeSend: function () {

                //IE11 bug fix for googlemaps (delete all google map instances)
                //check if the page is ajax = true, has google map class and the container is #content
                if (App.SmartAdmin.Config.Global.navAsAjax && $(".google_maps")[0] && (container[0] == $("#content")[0])) {

                    // target gmaps if any on page
                    var collection = $(".google_maps"),
                        i = 0;
                    // run for each	map
                    collection.each(function () {
                        i++;
                        // get map id from class elements
                        var divDealerMap = document.getElementById(this.id);

                        if (i == collection.length + 1) {
                            // "callback"
                        } else {
                            // destroy every map found
                            if (divDealerMap) divDealerMap.parentNode.removeChild(divDealerMap);

                            // debugState
                            if (debugState) {
                                App.SmartAdmin.Config.Global.global.console.log("Destroying maps.........%c" + this.id, debugStyle_warning);
                            }
                        }
                    });

                    // debugState
                    if (debugState) {
                        App.SmartAdmin.Config.Global.global.console.log("? Google map instances nuked!!!");
                    }

                } //end fix

                // destroy all datatable instances
                if (App.SmartAdmin.Config.Global.navAsAjax && $('.dataTables_wrapper')[0] && (container[0] == $("#content")[0])) {

                    var tables = $.fn.dataTable.fnTables(true);
                    $(tables).each(function () {

                        if ($(this).find('.details-control').length != 0) {
                            $(this).find('*').addBack().off().remove();
                            $(this).dataTable().fnDestroy();
                        } else {
                            $(this).dataTable().fnDestroy();
                        }

                    });

                    // debugState
                    if (App.SmartAdmin.Config.Global.debugState) {
                        App.SmartAdmin.Config.Global.global.console.log("? Datatable instances nuked!!!");
                    }
                }
                // end destroy

                // pop intervals (destroys jarviswidget related intervals)
                if (App.SmartAdmin.Config.Global.navAsAjax && $.intervalArr.length > 0 && (container[0] == $("#content")[0]) && enableJarvisWidgets) {

                    while ($.intervalArr.length > 0)
                        clearInterval($.intervalArr.pop());
                    // debugState
                    if (App.SmartAdmin.Config.Global.debugState) {
                        App.SmartAdmin.Config.Global.global.console.log("? All JarvisWidget intervals cleared");
                    }

                }
                // end pop intervals

                // destroy all widget instances
                if (App.SmartAdmin.Config.Global.navAsAjax && (container[0] == $("#content")[0]) && enableJarvisWidgets && $("#widget-grid")[0]) {

                    $("#widget-grid").jarvisWidgets('destroy');
                    // debugState
                    if (App.SmartAdmin.Config.Global.debugState) {
                        App.SmartAdmin.Config.Global.global.console.log("? JarvisWidgets destroyed");
                    }

                }
                // end destroy all widgets

                // cluster destroy: destroy other instances that could be on the page
                // this runs a script in the current loaded page before fetching the new page
                if (App.SmartAdmin.Config.Global.navAsAjax && (container[0] == $("#content")[0])) {

                    /*
                     * The following elements should be removed, if they have been created:
                     *
                     *	colorList
                     *	icon
                     *	picker
                     *	inline
                     *	And unbind events from elements:
                     *
                     *	icon
                     *	picker
                     *	inline
                     *	especially $(document).on('mousedown')
                     *	It will be much easier to add namespace to plugin events and then unbind using selected namespace.
                     *
                     *	See also:
                     *
                     *	http://f6design.com/journal/2012/05/06/a-jquery-plugin-boilerplate/
                     *	http://keith-wood.name/pluginFramework.html
                     */

                    // this function is below the pagefunction for all pages that has instances

                    if (typeof pagedestroy == 'function') {

                        try {
                            pagedestroy();

                            if (App.SmartAdmin.Config.Global.debugState) {
                                App.SmartAdmin.Config.Global.global.console.log("? Pagedestroy()");
                            }
                        }
                        catch (err) {
                            pagedestroy = undefined;

                            if (App.SmartAdmin.Config.Global.debugState) {
                                App.SmartAdmin.Config.Global.global.console.log("! Pagedestroy() Catch Error");
                            }
                        }

                    }

                    // destroy all inline charts

                    if ($.fn.sparkline && $("#content .sparkline")[0]) {
                        $("#content .sparkline").sparkline('destroy');

                        if (App.SmartAdmin.Config.Global.debugState) {
                            App.SmartAdmin.Config.Global.global.console.log("? Sparkline Charts destroyed!");
                        }
                    }

                    if ($.fn.easyPieChart && $("#content .easy-pie-chart")[0]) {
                        $("#content .easy-pie-chart").easyPieChart('destroy');

                        if (App.SmartAdmin.Config.Global.debugState) {
                            App.SmartAdmin.Config.Global.global.console.log("? EasyPieChart Charts destroyed!");
                        }
                    }


                    // end destory all inline charts

                    // destroy form controls: Datepicker, select2, autocomplete, mask, bootstrap slider

                    if ($.fn.select2 && $("#content select.select2")[0]) {
                        $("#content select.select2").select2('destroy');

                        if (App.SmartAdmin.Config.Global.debugState) {
                            App.SmartAdmin.Config.Global.global.console.log("? Select2 destroyed!");
                        }
                    }

                    if ($.fn.mask && $('#content [data-mask]')[0]) {
                        $('#content [data-mask]').unmask();

                        if (App.SmartAdmin.Config.Global.debugState) {
                            App.SmartAdmin.Config.Global.global.console.log("? Input Mask destroyed!");
                        }
                    }

                    if ($.fn.datepicker && $('#content .datepicker')[0]) {
                        $('#content .datepicker').off();
                        $('#content .datepicker').remove();

                        if (App.SmartAdmin.Config.Global.debugState) {
                            App.SmartAdmin.Config.Global.global.console.log("? Datepicker destroyed!");
                        }
                    }

                    if ($.fn.slider && $('#content .slider')[0]) {
                        $('#content .slider').off();
                        $('#content .slider').remove();

                        if (App.SmartAdmin.Config.Global.debugState) {
                            App.SmartAdmin.Config.Global.global.console.log("? Bootstrap Slider destroyed!");
                        }
                    }

                    // end destroy form controls


                }
                // end cluster destroy

                // empty container and var to start garbage collection (frees memory)
                pagefunction = null;
                container.removeData().html("");

                // place cog
                container.html('<h1 class="ajax-loading-animation"><i class="fa fa-cog fa-spin"></i> Loading...</h1>');

                // Only draw breadcrumb if it is main content material
                if (container[0] == $("#content")[0]) {

                    // clear everything else except these key DOM elements
                    // we do this because sometime plugins will leave dynamic elements behind
                    $('body').find('> *').filter(':not(' + ignore_key_elms + ')').empty().remove();

                    // draw breadcrumb
                    drawBreadCrumb();

                    // scroll up
                    $("html").animate({
                        scrollTop: 0
                    }, "fast");
                }
                // end if
            },
            success: function (data) {

                // dump data to container
                container.css({
                    opacity: '0.0'
                }).html(data).delay(50).animate({
                    opacity: '1.0'
                }, 300);

                // clear data var
                data = null;
                container = null;
            },
            error: function (xhr, status, thrownError, error) {
                container.html('<h4 class="ajax-loading-error"><i class="fa fa-warning txt-color-orangeDark"></i> Error requesting <span class="txt-color-red">' + url + '</span>: ' + xhr.status + ' <span style="text-transform: capitalize;">' + thrownError + '</span></h4>');
            },
            async: true
        });

    },
    getParam: function (name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else
            return results[1];
    }
});
