Namespace("SmartAdmin.Core", {
    initApp: function () {
        var app = {};

        /*
         * ADD DEVICE TYPE
         * Detect if mobile or desktop
         */
        app.addDeviceType = function () {

            if (!App.SmartAdmin.Config.Global.ismobile) {
                // Desktop
                $('body').addClass("desktop-detected");
                App.SmartAdmin.Config.Global.thisDevice = "desktop";
                return false;
            } else {
                // Mobile
                $('body').addClass("mobile-detected");
                App.SmartAdmin.Config.Global.thisDevice = "mobile";

                if (FastClick) {
                    // Removes the tap delay in idevices
                    // dependency: js/plugin/fastclick/fastclick.js
                    $('body').addClass("needsclick");
                    FastClick.attach(document.body);
                    return false;
                }

            }
        };
        /* ~ END: ADD DEVICE TYPE */
        /*
         * CHECK FOR MENU POSITION
         * Scans localstroage for menu position (vertical or horizontal)
         */
        app.menuPos = function () {

            if ($('body').hasClass("menu-on-top") || localStorage.getItem('sm-setmenu') == 'top') {
                topmenu = true;
                $('body').addClass("menu-on-top");
            }
        };

        app.SmartActions = function () {
            var smartActions = {
                // LOGOUT MSG
                userLogout: function ($this) {
                    // ask verification
                    $.SmartMessageBox({
                        title: "<i class='fa fa-sign-out txt-color-orangeDark'></i> Logout <span class='txt-color-orangeDark'><strong>" + $('#show-shortcut').text() + "</strong></span> ?",
                        content: $this.data('logout-msg') || "You can improve your security further after logging out by closing this opened browser",
                        buttons: '[No][Yes]'

                    }, function (ButtonPressed) {
                        if (ButtonPressed == "Yes") {
                            $('body').addClass('animated fadeOutUp');
                            setTimeout(logout, 1000);
                        }
                    });
                    function logout() {
                        window.location = $this.attr('href');
                    }

                },

                // RESET WIDGETS
                resetWidgets: function ($this) {

                    $.SmartMessageBox({
                        title: "<i class='fa fa-refresh' style='color:green'></i> Clear Local Storage",
                        content: $this.data('reset-msg') || "Would you like to RESET all your saved widgets and clear LocalStorage?1",
                        buttons: '[No][Yes]'
                    }, function (ButtonPressed) {
                        if (ButtonPressed == "Yes" && localStorage) {
                            localStorage.clear();
                            location.reload();
                        }

                    });
                },

                // LAUNCH FULLSCREEN
                launchFullscreen: function (element) {

                    if (!$('body').hasClass("full-screen")) {

                        $('body').addClass("full-screen");

                        if (element.requestFullscreen) {
                            element.requestFullscreen();
                        } else if (element.mozRequestFullScreen) {
                            element.mozRequestFullScreen();
                        } else if (element.webkitRequestFullscreen) {
                            element.webkitRequestFullscreen();
                        } else if (element.msRequestFullscreen) {
                            element.msRequestFullscreen();
                        }

                    } else {

                        $('body').removeClass("full-screen");

                        if (document.exitFullscreen) {
                            document.exitFullscreen();
                        } else if (document.mozCancelFullScreen) {
                            document.mozCancelFullScreen();
                        } else if (document.webkitExitFullscreen) {
                            document.webkitExitFullscreen();
                        }

                    }

                },

                // MINIFY MENU
                minifyMenu: function ($this) {
                    if (!$('body').hasClass("menu-on-top")) {
                        $('body').toggleClass("minified");
                        $('body').removeClass("hidden-menu");
                        $('html').removeClass("hidden-menu-mobile-lock");
                        $this.effect("highlight", {}, 500);
                    }
                },

                // TOGGLE MENU
                toggleMenu: function () {
                    if (!$('body').hasClass("menu-on-top")) {
                        $('html').toggleClass("hidden-menu-mobile-lock");
                        $('body').toggleClass("hidden-menu");
                        $('body').removeClass("minified");
                        //} else if ( $.root_.hasClass("menu-on-top") && $.root_.hasClass("mobile-view-activated") ) {
                        // suggested fix from Christian J�ger
                    } else if ($('body').hasClass("menu-on-top") && $(window).width() < 979) {
                        $('html').toggleClass("hidden-menu-mobile-lock");
                        $('body').toggleClass("hidden-menu");
                        $('body').removeClass("minified");
                    }
                },

                // TOGGLE SHORTCUT
                toggleShortcut: function () {

                    if ($('#shortcut').is(":visible")) {
                        shortcut_buttons_hide();
                    } else {
                        shortcut_buttons_show();
                    }

                    // SHORT CUT (buttons that appear when clicked on user name)
                    $('#shortcut').find('a').click(function (e) {
                        e.preventDefault();
                        window.location = $(this).attr('href');
                        setTimeout(shortcut_buttons_hide, 300);

                    });

                    // SHORTCUT buttons goes away if mouse is clicked outside of the area
                    $(document).mouseup(function (e) {
                        if (!$('#shortcut').is(e.target) && $('#shortcut').has(e.target).length === 0) {
                            shortcut_buttons_hide();
                        }
                    });

                    // SHORTCUT ANIMATE HIDE
                    function shortcut_buttons_hide() {
                        $('#shortcut').animate({
                            height: "hide"
                        }, 300, "easeOutCirc");
                        $('body').removeClass('shortcut-on');

                    }

                    // SHORTCUT ANIMATE SHOW
                    function shortcut_buttons_show() {
                        $('#shortcut').animate({
                            height: "show"
                        }, 200, "easeOutCirc");
                        $('body').addClass('shortcut-on');
                    }

                }
            }

            return smartActions;
        };

        /*
         * ACTIVATE NAVIGATION
         * Description: Activation will fail if top navigation is on
         */
        app.leftNav = function () {

            // INITIALIZE LEFT NAV
            if (!App.SmartAdmin.Config.Global.topmenu) {
                if (!null) {
                    $('nav ul').jarvismenu({
                        accordion: App.SmartAdmin.Config.Global.menu_accordion || true,
                        speed: App.SmartAdmin.Config.Global.menu_speed || true,
                        closedSign: '<em class="fa fa-plus-square-o"></em>',
                        openedSign: '<em class="fa fa-minus-square-o"></em>'
                    });
                } else {
                    alert("Error - menu anchor does not exist");
                }
            }

        };

        /*
         * MISCELANEOUS DOM READY FUNCTIONS
         * Description: fire with jQuery(document).ready...
         */
        app.domReadyMisc = function () {

            /*
             * FIRE TOOLTIPS
             */
            if ($("[rel=tooltip]").length) {
                $("[rel=tooltip]").tooltip();
            }

            // SHOW & HIDE MOBILE SEARCH FIELD
            $('#search-mobile').click(function () {
                $('body').addClass('search-mobile');
            });

            $('#cancel-search-js').click(function () {
                $('body').removeClass('search-mobile');
            });

            // ACTIVITY
            // ajax drop
            $('#activity').click(function (e) {
                var $this = $(this);

                if ($this.find('.badge').hasClass('bg-color-red')) {
                    $this.find('.badge').removeClassPrefix('bg-color-');
                    $this.find('.badge').text("0");
                }

                if (!$this.next('.ajax-dropdown').is(':visible')) {
                    $this.next('.ajax-dropdown').fadeIn(150);
                    $this.addClass('active');
                } else {
                    $this.next('.ajax-dropdown').fadeOut(150);
                    $this.removeClass('active');
                }

                var theUrlVal = $this.next('.ajax-dropdown').find('.btn-group > .active > input').attr('id');

                //clear memory reference
                $this = null;
                theUrlVal = null;

                e.preventDefault();
            });

            $('input[name="activity"]').change(function () {
                var $this = $(this);

                url = $this.attr('id');
                container = $('.ajax-notifications');

                App.SmartAdmin.Utils.loadURL(url, container);

                //clear memory reference
                $this = null;
            });

            // close dropdown if mouse is not inside the area of .ajax-dropdown
            $(document).mouseup(function (e) {
                if (!$('.ajax-dropdown').is(e.target) && $('.ajax-dropdown').has(e.target).length === 0) {
                    $('.ajax-dropdown').fadeOut(150);
                    $('.ajax-dropdown').prev().removeClass("active");
                }
            });

            // loading animation (demo purpose only)
            $('button[data-btn-loading]').on('click', function () {
                var btn = $(this);
                btn.button('loading');
                setTimeout(function () {
                    btn.button('reset');
                }, 3000);
            });

            // NOTIFICATION IS PRESENT
            // Change color of lable once notification button is clicked

            $this = $('#activity > .badge');

            if (parseInt($this.text()) > 0) {
                $this.addClass("bg-color-red bounceIn animated");

                //clear memory reference
                $this = null;
            }

            /*
             * ONE POP OVER THEORY
             * Keep only 1 active popover per trigger - also check and hide active popover if user clicks on document
             */
            $('body').on('click', function (e) {
                $('[rel="popover"], [data-rel="popover"]').each(function () {
                    //the 'is' for buttons that trigger popups
                    //the 'has' for icons within a button that triggers a popup
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });
            /* ~ END: ONE POP OVER THEORY */

            /*
             * DELETE MODEL DATA ON HIDDEN
             * Clears the model data once it is hidden, this way you do not create duplicated data on multiple modals
             */
            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
            /* ~ END: DELETE MODEL DATA ON HIDDEN */


        };

        app.mobileCheckActivation = function () {
            if ($(window).width() < 981) {
                console.log('mobile');
                $('body').addClass('mobile-view-activated');
                $('body').removeClass('minified');
            } else if ($('body').hasClass('mobile-view-activated')) {
                $('body').removeClass('mobile-view-activated');
                console.log('desktop');
            }

            if (App.SmartAdmin.Config.Global.debugState) {
                console.log('mobile');
                console.log("mobileCheckActivation");
            }

        }

        return app;
    },

    setup_widgets_desktop: function () {
        if ($.fn.jarvisWidgets && App.SmartAdmin.Config.Global.enableJarvisWidgets) {

            $('#widget-grid').jarvisWidgets({

                grid: 'article',
                widgets: '.jarviswidget',
                localStorage: App.SmartAdmin.Config.Global.localStorageJarvisWidgets,
                deleteSettingsKey: '#deletesettingskey-options',
                settingsKeyLabel: 'Reset settings?',
                deletePositionKey: '#deletepositionkey-options',
                positionKeyLabel: 'Reset position?',
                sortable: App.SmartAdmin.Config.Global.sortableJarvisWidgets,
                buttonsHidden: false,
                // toggle button
                toggleButton: true,
                toggleClass: 'fa fa-minus | fa fa-plus',
                toggleSpeed: 200,
                onToggle: function () {
                },
                // delete btn
                deleteButton: true,
                deleteMsg: 'Warning: This action cannot be undone!',
                deleteClass: 'fa fa-times',
                deleteSpeed: 200,
                onDelete: function () {
                },
                // edit btn
                editButton: true,
                editPlaceholder: '.jarviswidget-editbox',
                editClass: 'fa fa-cog | fa fa-save',
                editSpeed: 200,
                onEdit: function () {
                },
                // color button
                colorButton: true,
                // full screen
                fullscreenButton: true,
                fullscreenClass: 'fa fa-expand | fa fa-compress',
                fullscreenDiff: 3,
                onFullscreen: function () {
                },
                // custom btn
                customButton: false,
                customClass: 'folder-10 | next-10',
                customStart: function () {
                    alert('Hello you, this is a custom button...');
                },
                customEnd: function () {
                    alert('bye, till next time...');
                },
                // order
                buttonOrder: '%refresh% %custom% %edit% %toggle% %fullscreen% %delete%',
                opacity: 1.0,
                dragHandle: '> header',
                placeholderClass: 'jarviswidget-placeholder',
                indicator: true,
                indicatorTime: 600,
                ajax: true,
                timestampPlaceholder: '.jarviswidget-timestamp',
                timestampFormat: 'Last update: %m%/%d%/%y% %h%:%i%:%s%',
                refreshButton: true,
                refreshButtonClass: 'fa fa-refresh',
                labelError: 'Sorry but there was a error:',
                labelUpdated: 'Last Update:',
                labelRefresh: 'Refresh',
                labelDelete: 'Delete widget:',
                afterLoad: function () {
                },
                rtl: false, // best not to toggle this!
                onChange: function () {

                },
                onSave: function () {

                },
                ajaxnav: App.SmartAdmin.Config.Global.navAsAjax // declears how the localstorage should be saved
                // (HTML or AJAX Version)

            });

        }
    },

    setup_widgets_mobile: function () {

        if (App.SmartAdmin.Config.Global.enableMobileWidgets && App.SmartAdmin.Config.Global.enableJarvisWidgets) {
            App.SmartAdmin.Config.Global.setup_widgets_desktop();
        }

    },

    runAllCharts: function () {
        if ($.fn.sparkline) {

            // variable declearations:

            var barColor,
                sparklineHeight,
                sparklineBarWidth,
                sparklineBarSpacing,
                sparklineNegBarColor,
                sparklineStackedColor,
                thisLineColor,
                thisLineWidth,
                thisFill,
                thisSpotColor,
                thisMinSpotColor,
                thisMaxSpotColor,
                thishighlightSpotColor,
                thisHighlightLineColor,
                thisSpotRadius,
                pieColors,
                pieWidthHeight,
                pieBorderColor,
                pieOffset,
                thisBoxWidth,
                thisBoxHeight,
                thisBoxRaw,
                thisBoxTarget,
                thisBoxMin,
                thisBoxMax,
                thisShowOutlier,
                thisIQR,
                thisBoxSpotRadius,
                thisBoxLineColor,
                thisBoxFillColor,
                thisBoxWhisColor,
                thisBoxOutlineColor,
                thisBoxOutlineFill,
                thisBoxMedianColor,
                thisBoxTargetColor,
                thisBulletHeight,
                thisBulletWidth,
                thisBulletColor,
                thisBulletPerformanceColor,
                thisBulletRangeColors,
                thisDiscreteHeight,
                thisDiscreteWidth,
                thisDiscreteLineColor,
                thisDiscreteLineHeight,
                thisDiscreteThrushold,
                thisDiscreteThrusholdColor,
                thisTristateHeight,
                thisTristatePosBarColor,
                thisTristateNegBarColor,
                thisTristateZeroBarColor,
                thisTristateBarWidth,
                thisTristateBarSpacing,
                thisZeroAxis,
                thisBarColor,
                sparklineWidth,
                sparklineValue,
                sparklineValueSpots1,
                sparklineValueSpots2,
                thisLineWidth1,
                thisLineWidth2,
                thisLineColor1,
                thisLineColor2,
                thisSpotRadius1,
                thisSpotRadius2,
                thisMinSpotColor1,
                thisMaxSpotColor1,
                thisMinSpotColor2,
                thisMaxSpotColor2,
                thishighlightSpotColor1,
                thisHighlightLineColor1,
                thishighlightSpotColor2,
                thisFillColor1,
                thisFillColor2;

            $('.sparkline:not(:has(>canvas))').each(function () {
                var $this = $(this),
                    sparklineType = $this.data('sparkline-type') || 'bar';

                // BAR CHART
                if (sparklineType == 'bar') {

                    barColor = $this.data('sparkline-bar-color') || $this.css('color') || '#0000f0';
                    sparklineHeight = $this.data('sparkline-height') || '26px';
                    sparklineBarWidth = $this.data('sparkline-barwidth') || 5;
                    sparklineBarSpacing = $this.data('sparkline-barspacing') || 2;
                    sparklineNegBarColor = $this.data('sparkline-negbar-color') || '#A90329';
                    sparklineStackedColor = $this.data('sparkline-barstacked-color') || ["#A90329", "#0099c6", "#98AA56", "#da532c", "#4490B1", "#6E9461", "#990099", "#B4CAD3"];

                    $this.sparkline('html', {
                        barColor: barColor,
                        type: sparklineType,
                        height: sparklineHeight,
                        barWidth: sparklineBarWidth,
                        barSpacing: sparklineBarSpacing,
                        stackedBarColor: sparklineStackedColor,
                        negBarColor: sparklineNegBarColor,
                        zeroAxis: 'false'
                    });

                    $this = null;

                }

                // LINE CHART
                if (sparklineType == 'line') {

                    sparklineHeight = $this.data('sparkline-height') || '20px';
                    sparklineWidth = $this.data('sparkline-width') || '90px';
                    thisLineColor = $this.data('sparkline-line-color') || $this.css('color') || '#0000f0';
                    thisLineWidth = $this.data('sparkline-line-width') || 1;
                    thisFill = $this.data('fill-color') || '#c0d0f0';
                    thisSpotColor = $this.data('sparkline-spot-color') || '#f08000';
                    thisMinSpotColor = $this.data('sparkline-minspot-color') || '#ed1c24';
                    thisMaxSpotColor = $this.data('sparkline-maxspot-color') || '#f08000';
                    thishighlightSpotColor = $this.data('sparkline-highlightspot-color') || '#50f050';
                    thisHighlightLineColor = $this.data('sparkline-highlightline-color') || 'f02020';
                    thisSpotRadius = $this.data('sparkline-spotradius') || 1.5;
                    thisChartMinYRange = $this.data('sparkline-min-y') || 'undefined';
                    thisChartMaxYRange = $this.data('sparkline-max-y') || 'undefined';
                    thisChartMinXRange = $this.data('sparkline-min-x') || 'undefined';
                    thisChartMaxXRange = $this.data('sparkline-max-x') || 'undefined';
                    thisMinNormValue = $this.data('min-val') || 'undefined';
                    thisMaxNormValue = $this.data('max-val') || 'undefined';
                    thisNormColor = $this.data('norm-color') || '#c0c0c0';
                    thisDrawNormalOnTop = $this.data('draw-normal') || false;

                    $this.sparkline('html', {
                        type: 'line',
                        width: sparklineWidth,
                        height: sparklineHeight,
                        lineWidth: thisLineWidth,
                        lineColor: thisLineColor,
                        fillColor: thisFill,
                        spotColor: thisSpotColor,
                        minSpotColor: thisMinSpotColor,
                        maxSpotColor: thisMaxSpotColor,
                        highlightSpotColor: thishighlightSpotColor,
                        highlightLineColor: thisHighlightLineColor,
                        spotRadius: thisSpotRadius,
                        chartRangeMin: thisChartMinYRange,
                        chartRangeMax: thisChartMaxYRange,
                        chartRangeMinX: thisChartMinXRange,
                        chartRangeMaxX: thisChartMaxXRange,
                        normalRangeMin: thisMinNormValue,
                        normalRangeMax: thisMaxNormValue,
                        normalRangeColor: thisNormColor,
                        drawNormalOnTop: thisDrawNormalOnTop

                    });

                    $this = null;

                }

                // PIE CHART
                if (sparklineType == 'pie') {

                    pieColors = $this.data('sparkline-piecolor') || ["#B4CAD3", "#4490B1", "#98AA56", "#da532c", "#6E9461", "#0099c6", "#990099", "#717D8A"];
                    pieWidthHeight = $this.data('sparkline-piesize') || 90;
                    pieBorderColor = $this.data('border-color') || '#45494C';
                    pieOffset = $this.data('sparkline-offset') || 0;

                    $this.sparkline('html', {
                        type: 'pie',
                        width: pieWidthHeight,
                        height: pieWidthHeight,
                        tooltipFormat: '<span style="color: {{color}}">&#9679;</span> ({{percent.1}}%)',
                        sliceColors: pieColors,
                        borderWidth: 1,
                        offset: pieOffset,
                        borderColor: pieBorderColor
                    });

                    $this = null;

                }

                // BOX PLOT
                if (sparklineType == 'box') {

                    thisBoxWidth = $this.data('sparkline-width') || 'auto';
                    thisBoxHeight = $this.data('sparkline-height') || 'auto';
                    thisBoxRaw = $this.data('sparkline-boxraw') || false;
                    thisBoxTarget = $this.data('sparkline-targetval') || 'undefined';
                    thisBoxMin = $this.data('sparkline-min') || 'undefined';
                    thisBoxMax = $this.data('sparkline-max') || 'undefined';
                    thisShowOutlier = $this.data('sparkline-showoutlier') || true;
                    thisIQR = $this.data('sparkline-outlier-iqr') || 1.5;
                    thisBoxSpotRadius = $this.data('sparkline-spotradius') || 1.5;
                    thisBoxLineColor = $this.css('color') || '#000000';
                    thisBoxFillColor = $this.data('fill-color') || '#c0d0f0';
                    thisBoxWhisColor = $this.data('sparkline-whis-color') || '#000000';
                    thisBoxOutlineColor = $this.data('sparkline-outline-color') || '#303030';
                    thisBoxOutlineFill = $this.data('sparkline-outlinefill-color') || '#f0f0f0';
                    thisBoxMedianColor = $this.data('sparkline-outlinemedian-color') || '#f00000';
                    thisBoxTargetColor = $this.data('sparkline-outlinetarget-color') || '#40a020';

                    $this.sparkline('html', {
                        type: 'box',
                        width: thisBoxWidth,
                        height: thisBoxHeight,
                        raw: thisBoxRaw,
                        target: thisBoxTarget,
                        minValue: thisBoxMin,
                        maxValue: thisBoxMax,
                        showOutliers: thisShowOutlier,
                        outlierIQR: thisIQR,
                        spotRadius: thisBoxSpotRadius,
                        boxLineColor: thisBoxLineColor,
                        boxFillColor: thisBoxFillColor,
                        whiskerColor: thisBoxWhisColor,
                        outlierLineColor: thisBoxOutlineColor,
                        outlierFillColor: thisBoxOutlineFill,
                        medianColor: thisBoxMedianColor,
                        targetColor: thisBoxTargetColor

                    });

                    $this = null;

                }

                // BULLET
                if (sparklineType == 'bullet') {

                    var thisBulletHeight = $this.data('sparkline-height') || 'auto';
                    thisBulletWidth = $this.data('sparkline-width') || 2;
                    thisBulletColor = $this.data('sparkline-bullet-color') || '#ed1c24';
                    thisBulletPerformanceColor = $this.data('sparkline-performance-color') || '#3030f0';
                    thisBulletRangeColors = $this.data('sparkline-bulletrange-color') || ["#d3dafe", "#a8b6ff", "#7f94ff"];

                    $this.sparkline('html', {

                        type: 'bullet',
                        height: thisBulletHeight,
                        targetWidth: thisBulletWidth,
                        targetColor: thisBulletColor,
                        performanceColor: thisBulletPerformanceColor,
                        rangeColors: thisBulletRangeColors

                    });

                    $this = null;

                }

                // DISCRETE
                if (sparklineType == 'discrete') {

                    thisDiscreteHeight = $this.data('sparkline-height') || 26;
                    thisDiscreteWidth = $this.data('sparkline-width') || 50;
                    thisDiscreteLineColor = $this.css('color');
                    thisDiscreteLineHeight = $this.data('sparkline-line-height') || 5;
                    thisDiscreteThrushold = $this.data('sparkline-threshold') || 'undefined';
                    thisDiscreteThrusholdColor = $this.data('sparkline-threshold-color') || '#ed1c24';

                    $this.sparkline('html', {

                        type: 'discrete',
                        width: thisDiscreteWidth,
                        height: thisDiscreteHeight,
                        lineColor: thisDiscreteLineColor,
                        lineHeight: thisDiscreteLineHeight,
                        thresholdValue: thisDiscreteThrushold,
                        thresholdColor: thisDiscreteThrusholdColor

                    });

                    $this = null;

                }

                // TRISTATE
                if (sparklineType == 'tristate') {

                    thisTristateHeight = $this.data('sparkline-height') || 26;
                    thisTristatePosBarColor = $this.data('sparkline-posbar-color') || '#60f060';
                    thisTristateNegBarColor = $this.data('sparkline-negbar-color') || '#f04040';
                    thisTristateZeroBarColor = $this.data('sparkline-zerobar-color') || '#909090';
                    thisTristateBarWidth = $this.data('sparkline-barwidth') || 5;
                    thisTristateBarSpacing = $this.data('sparkline-barspacing') || 2;
                    thisZeroAxis = $this.data('sparkline-zeroaxis') || false;

                    $this.sparkline('html', {

                        type: 'tristate',
                        height: thisTristateHeight,
                        posBarColor: thisBarColor,
                        negBarColor: thisTristateNegBarColor,
                        zeroBarColor: thisTristateZeroBarColor,
                        barWidth: thisTristateBarWidth,
                        barSpacing: thisTristateBarSpacing,
                        zeroAxis: thisZeroAxis

                    });

                    $this = null;

                }

                //COMPOSITE: BAR
                if (sparklineType == 'compositebar') {

                    sparklineHeight = $this.data('sparkline-height') || '20px';
                    sparklineWidth = $this.data('sparkline-width') || '100%';
                    sparklineBarWidth = $this.data('sparkline-barwidth') || 3;
                    thisLineWidth = $this.data('sparkline-line-width') || 1;
                    thisLineColor = $this.data('data-sparkline-linecolor') || '#ed1c24';
                    thisBarColor = $this.data('data-sparkline-barcolor') || '#333333';

                    $this.sparkline($this.data('sparkline-bar-val'), {

                        type: 'bar',
                        width: sparklineWidth,
                        height: sparklineHeight,
                        barColor: thisBarColor,
                        barWidth: sparklineBarWidth
                        //barSpacing: 5

                    });

                    $this.sparkline($this.data('sparkline-line-val'), {

                        width: sparklineWidth,
                        height: sparklineHeight,
                        lineColor: thisLineColor,
                        lineWidth: thisLineWidth,
                        composite: true,
                        fillColor: false

                    });

                    $this = null;

                }

                //COMPOSITE: LINE
                if (sparklineType == 'compositeline') {

                    sparklineHeight = $this.data('sparkline-height') || '20px';
                    sparklineWidth = $this.data('sparkline-width') || '90px';
                    sparklineValue = $this.data('sparkline-bar-val');
                    sparklineValueSpots1 = $this.data('sparkline-bar-val-spots-top') || null;
                    sparklineValueSpots2 = $this.data('sparkline-bar-val-spots-bottom') || null;
                    thisLineWidth1 = $this.data('sparkline-line-width-top') || 1;
                    thisLineWidth2 = $this.data('sparkline-line-width-bottom') || 1;
                    thisLineColor1 = $this.data('sparkline-color-top') || '#333333';
                    thisLineColor2 = $this.data('sparkline-color-bottom') || '#ed1c24';
                    thisSpotRadius1 = $this.data('sparkline-spotradius-top') || 1.5;
                    thisSpotRadius2 = $this.data('sparkline-spotradius-bottom') || thisSpotRadius1;
                    thisSpotColor = $this.data('sparkline-spot-color') || '#f08000';
                    thisMinSpotColor1 = $this.data('sparkline-minspot-color-top') || '#ed1c24';
                    thisMaxSpotColor1 = $this.data('sparkline-maxspot-color-top') || '#f08000';
                    thisMinSpotColor2 = $this.data('sparkline-minspot-color-bottom') || thisMinSpotColor1;
                    thisMaxSpotColor2 = $this.data('sparkline-maxspot-color-bottom') || thisMaxSpotColor1;
                    thishighlightSpotColor1 = $this.data('sparkline-highlightspot-color-top') || '#50f050';
                    thisHighlightLineColor1 = $this.data('sparkline-highlightline-color-top') || '#f02020';
                    thishighlightSpotColor2 = $this.data('sparkline-highlightspot-color-bottom') ||
                        thishighlightSpotColor1;
                    thisHighlightLineColor2 = $this.data('sparkline-highlightline-color-bottom') ||
                        thisHighlightLineColor1;
                    thisFillColor1 = $this.data('sparkline-fillcolor-top') || 'transparent';
                    thisFillColor2 = $this.data('sparkline-fillcolor-bottom') || 'transparent';

                    $this.sparkline(sparklineValue, {

                        type: 'line',
                        spotRadius: thisSpotRadius1,

                        spotColor: thisSpotColor,
                        minSpotColor: thisMinSpotColor1,
                        maxSpotColor: thisMaxSpotColor1,
                        highlightSpotColor: thishighlightSpotColor1,
                        highlightLineColor: thisHighlightLineColor1,

                        valueSpots: sparklineValueSpots1,

                        lineWidth: thisLineWidth1,
                        width: sparklineWidth,
                        height: sparklineHeight,
                        lineColor: thisLineColor1,
                        fillColor: thisFillColor1

                    });

                    $this.sparkline($this.data('sparkline-line-val'), {

                        type: 'line',
                        spotRadius: thisSpotRadius2,

                        spotColor: thisSpotColor,
                        minSpotColor: thisMinSpotColor2,
                        maxSpotColor: thisMaxSpotColor2,
                        highlightSpotColor: thishighlightSpotColor2,
                        highlightLineColor: thisHighlightLineColor2,

                        valueSpots: sparklineValueSpots2,

                        lineWidth: thisLineWidth2,
                        width: sparklineWidth,
                        height: sparklineHeight,
                        lineColor: thisLineColor2,
                        composite: true,
                        fillColor: thisFillColor2

                    });

                    $this = null;

                }

            });

        }// end if

        /*
         * EASY PIE CHARTS
         * DEPENDENCY: js/plugins/easy-pie-chart/jquery.easy-pie-chart.min.js
         * Usage: <div class="easy-pie-chart txt-color-orangeDark" data-pie-percent="33" data-pie-size="72" data-size="72">
         *			<span class="percent percent-sign">35</span>
         * 	  	  </div>
         */

        if ($.fn.easyPieChart) {

            $('.easy-pie-chart').each(function () {
                var $this = $(this),
                    barColor = $this.css('color') || $this.data('pie-color'),
                    trackColor = $this.data('pie-track-color') || 'rgba(0,0,0,0.04)',
                    size = parseInt($this.data('pie-size')) || 25;

                $this.easyPieChart({

                    barColor: barColor,
                    trackColor: trackColor,
                    scaleColor: false,
                    lineCap: 'butt',
                    lineWidth: parseInt(size / 8.5),
                    animate: 1500,
                    rotate: -90,
                    size: size,
                    onStep: function (from, to, percent) {
                        $(this.el).find('.percent').text(Math.round(percent));
                    }

                });

                $this = null;
            });

        } // end if
    },

    runAllForms: function () {
        /*
         * BOOTSTRAP SLIDER PLUGIN
         * Usage:
         * Dependency: js/plugin/bootstrap-slider
         */
        if ($.fn.slider) {
            $('.slider').slider();
        }

        /*
         * SELECT2 PLUGIN
         * Usage:
         * Dependency: js/plugin/select2/
         */
        if ($.fn.select2) {
            $('select.select2').each(function () {
                var $this = $(this),
                    width = $this.attr('data-select-width') || '100%';
                //, _showSearchInput = $this.attr('data-select-search') === 'true';
                $this.select2({
                    //showSearchInput : _showSearchInput,
                    allowClear: true,
                    width: width
                });

                //clear memory reference
                $this = null;
            });
        }

        /*
         * MASKING
         * Dependency: js/plugin/masked-input/
         */
        if ($.fn.mask) {
            $('[data-mask]').each(function () {

                var $this = $(this),
                    mask = $this.attr('data-mask') || 'error...', mask_placeholder = $this.attr('data-mask-placeholder') || 'X';

                $this.mask(mask, {
                    placeholder: mask_placeholder
                });

                //clear memory reference
                $this = null;
            });
        }

        /*
         * AUTOCOMPLETE
         * Dependency: js/jqui
         */
        if ($.fn.autocomplete) {
            $('[data-autocomplete]').each(function () {

                var $this = $(this),
                    availableTags = $this.data('autocomplete') || ["The", "Quick", "Brown", "Fox", "Jumps", "Over", "Three", "Lazy", "Dogs"];

                $this.autocomplete({
                    source: availableTags
                });

                //clear memory reference
                $this = null;
            });
        }

        /*
         * JQUERY UI DATE
         * Dependency: js/libs/jquery-ui-1.10.3.min.js
         * Usage: <input class="datepicker" />
         */
        if ($.fn.datepicker) {
            $('.datepicker').each(function () {

                var $this = $(this),
                    dataDateFormat = $this.attr('data-dateformat') || 'dd.mm.yy';

                $this.datepicker({
                    dateFormat: dataDateFormat,
                    prevText: '<i class="fa fa-chevron-left"></i>',
                    nextText: '<i class="fa fa-chevron-right"></i>',
                });

                //clear memory reference
                $this = null;
            });
        }

        /*
         * AJAX BUTTON LOADING TEXT
         * Usage: <button type="button" data-loading-text="Loading..." class="btn btn-xs btn-default ajax-refresh"> .. </button>
         */
        $('button[data-loading-text]').on('click', function () {
            var btn = $(this);
            btn.button('loading');
            setTimeout(function () {
                btn.button('reset');
                //clear memory reference
                btn = null;
            }, 3000);

        });

        /*
         * ELEMENT EXIST OR NOT
         * Description: returns true or false
         * Usage: $('#myDiv').doesExist();
         */
        jQuery.fn.doesExist = function () {
            return jQuery(this).length > 0;
        };
        /* ~ END: ELEMENT EXIST OR NOT */

        $('#main').resize(function () {

            App.SmartAdmin.Core.initApp().mobileCheckActivation();

        });
    },

    pageSetUp: function () {

        if (App.SmartAdmin.Config.Global.thisDevice === "desktop") {
            // is desktop
            $('body').addClass("fixed-header fixed-navigation fixed-page-footer fixed-ribbon");

            // activate tooltips
            $("[rel=tooltip], [data-rel=tooltip]").tooltip();

            // activate popovers
            $("[rel=popover], [data-rel=popover]").popover();

            // activate popovers with hover states
            $("[rel=popover-hover], [data-rel=popover-hover]").popover({
                trigger: "hover"
            });

            // setup widgets
            App.SmartAdmin.Core.setup_widgets_desktop();

            // activate inline charts
            App.SmartAdmin.Core.runAllCharts();

            // run form elements
            App.SmartAdmin.Core.runAllForms();

        } else {

            // is mobile

            // activate popovers
            $("[rel=popover], [data-rel=popover]").popover();

            // activate popovers with hover states
            $("[rel=popover-hover], [data-rel=popover-hover]").popover({
                trigger: "hover"
            });

            // activate inline charts
            App.SmartAdmin.Core.runAllCharts();

            // setup widgets
            App.SmartAdmin.Core.setup_widgets_mobile();

            // run form elements
            App.SmartAdmin.Core.runAllForms();

        }

    },
    initGoogleMaps: function () {
        /*
         * GOOGLE MAPS
         * description: Append google maps to head dynamically (only execute for ajax version)
         * Loads at the begining for ajax pages
         */
        if (App.SmartAdmin.Config.Global.navAsAjax || $(".google_maps")) {
            var gMapsLoaded = false;
            window.gMapsCallback = function () {
                gMapsLoaded = true;
                $(window).trigger('gMapsLoaded');
            };
            window.loadGoogleMaps = function () {
                if (gMapsLoaded)
                    return window.gMapsCallback();
                var script_tag = document.createElement('script');
                script_tag.setAttribute("type", "text/javascript");
                script_tag.setAttribute("src", "http://maps.google.com/maps/api/js?sensor=false&callback=gMapsCallback");
                (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
            };
        }
        /* ~ END: GOOGLE MAPS */
    }
});
