/**
 * Created by martinegan on 19/10/2015.
 */

Template.SmartAdminNavigation.events({
    'click #show-shortcut': function (e) {
        App.SmartAdmin.Core.initApp().SmartActions().toggleShortcut();
        e.preventDefault();
    }
});

Template.SmartAdminNavigation.helpers({
    displayName: function(){
        if(Meteor.user().profile.display_name) {
            return Meteor.user().profile.display_name;
        }
    }
});
