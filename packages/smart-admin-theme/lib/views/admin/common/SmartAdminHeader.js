Template.SmartAdminHeader.events({
    'click #hide-menu a': function (e) {
        console.log('clicked');

        App.SmartAdmin.Core.initApp().SmartActions().toggleMenu();
        e.preventDefault();
    },
    'click #fullscreen a': function (e) {
        App.SmartAdmin.Core.initApp().SmartActions().launchFullscreen(document.documentElement);
        e.preventDefault();
    },
    'click #speech-btn a': function (e) {
        if ($('body').hasClass("voice-command-active")) {
            $.speechApp.stop();
            //$('#speech-btn > span > a > i').removeClass().addClass('fa fa-microphone-slash');
        } else {
            $.speechApp.start();
            //add popover
            $('#speech-btn .popover').fadeIn(350);
            //$('#speech-btn > span > a > i').removeClass().addClass('fa fa-microphone')

        }

        e.preventDefault();
    },
    'click #logout a': function(e){
        Meteor.logout();
        Session.set('loggedIn', false);
        FlowRouter.go("login");

        e.preventDefault();
    }
});
